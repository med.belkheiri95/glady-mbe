package com.glady.wedogifts;

import com.glady.wedogifts.model.Company;
import com.glady.wedogifts.model.Distribution;
import com.glady.wedogifts.model.Person;
import com.glady.wedogifts.repository.CompanyRepository;
import com.glady.wedogifts.repository.PersonRepository;
import com.glady.wedogifts.service.GladyService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class WedogiftsApplicationTests {


	@Autowired
	GladyService gladyService;

	@Autowired
	TestUtil testUtil;

	@MockBean
	PersonRepository personRepository;

	@MockBean
	CompanyRepository companyRepository;

	@Test
	void calculateUserBalanceTestIsValidTrue() throws IOException {
		List<Person> people = testUtil.getPeople();
		Person personSource = people.get(0);
		Mockito.when(personRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((personSource)));
		Person person = gladyService.calculateUserBalance("1");
		assertEquals(true, person.getAccount().get(0).getIsValid());
	}

	@Test
	void calculateUserBalanceTestIsValidFalse() throws IOException {
		List<Person> people = testUtil.getPeople();
		Person personSource = people.get(1);
		Mockito.when(personRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((personSource)));
		Person person = gladyService.calculateUserBalance("2");
		assertEquals(false, person.getAccount().get(0).getIsValid());
	}

	@Test
	void distributeDepositsTest() throws IOException {

		List<Person> people = testUtil.getPeople();
		List<Company> companies = testUtil.getCompanies();
		Distribution distributionData = testUtil.getDistribution();
		Person personSource = people.get(0);
		Company companySource = companies.get(0);
		Mockito.when(personRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((personSource)));
		Mockito.when(companyRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((companySource)));
		assertEquals("distribution ok", gladyService.distributeDeposits(distributionData));
	}

	@Test
	void distributeDepositsTestKo() throws IOException {

		List<Person> people = testUtil.getPeople();
		List<Company> companies = testUtil.getCompanies();
		Distribution distributionData = testUtil.getDistribution();
		Person personSource = people.get(0);
		Company companySource = companies.get(0);
		distributionData.getBalance().setAmount(1000000);
		Mockito.when(personRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((personSource)));
		Mockito.when(companyRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((companySource)));
		assertEquals("company is broke", gladyService.distributeDeposits(distributionData));
	}

	@Test
	void distributeDepositsTestKoType() throws IOException {

		List<Person> people = testUtil.getPeople();
		List<Company> companies = testUtil.getCompanies();
		Distribution distributionData = testUtil.getDistribution();
		Person personSource = people.get(0);
		Company companySource = companies.get(1);
		distributionData.getBalance().setAmount(1000);
		Mockito.when(personRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((personSource)));
		Mockito.when(companyRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((companySource)));
		assertEquals("company doesnt support this type of deposit", gladyService.distributeDeposits(distributionData));
	}

	@Test
	void distributeDepositsAndCalculateTest() throws IOException {

		List<Person> people = testUtil.getPeople();
		List<Company> companies = testUtil.getCompanies();
		Distribution distributionData = testUtil.getDistribution();
		Person personSource = people.get(1);
		Company companySource = companies.get(0);
		Mockito.when(personRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((personSource)));
		Mockito.when(companyRepository.findById(Mockito.anyLong())).thenReturn(Optional.of((companySource)));
		assertEquals("distribution ok", gladyService.distributeDeposits(distributionData));

		assertEquals(false, gladyService.calculateUserBalance("2").getAccount().get(0).getIsValid());

		assertEquals(true, gladyService.calculateUserBalance("2").getAccount().get(1).getIsValid());
	}



}
