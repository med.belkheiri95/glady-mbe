package com.glady.wedogifts;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.glady.wedogifts.model.Company;
import com.glady.wedogifts.model.Distribution;
import com.glady.wedogifts.model.Person;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class TestUtil {

    private static final String PERSON_FILE = "./src/test/resources/person.json";
    private static final String COMPANIES_FILE = "./src/test/resources/companies.json";
    private static final String DISTRIBUTION_FILE = "./src/test/resources/distribution.json";
    private ObjectMapper m = new ObjectMapper();

    public List<Person> getPeople() throws IOException {
        return m.readValue(Files.readAllBytes(Paths.get(PERSON_FILE)), new TypeReference<List<Person>>() {});
    }

    public List<Company> getCompanies() throws IOException {
        return m.readValue(Files.readAllBytes(Paths.get(COMPANIES_FILE)), new TypeReference<List<Company>>() {});
    }

    public Distribution getDistribution() throws IOException {
        return m.readValue(Files.readAllBytes(Paths.get(DISTRIBUTION_FILE)), Distribution.class);
    }
}
