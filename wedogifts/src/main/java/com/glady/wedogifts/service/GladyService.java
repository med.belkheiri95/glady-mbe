package com.glady.wedogifts.service;

import com.glady.wedogifts.model.*;
import com.glady.wedogifts.repository.BalanceRepository;
import com.glady.wedogifts.repository.CompanyRepository;
import com.glady.wedogifts.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class GladyService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    BalanceRepository balanceRepository;

    private Date today = new Date(System.currentTimeMillis());

    public Person calculateUserBalance(String id){
        Person person = personRepository.findById(Long.parseLong(id)).get();
        List<Balance> balances = new ArrayList<>();
        for (Balance balance : person.getAccount()){
            // we could imagine a factory pattern
            // with dynamic polymorphism depending on balance type
            switch (balance.getType()){
                case FOOD:
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, today.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusYears(1).getYear());
                    calendar.set(Calendar.MONTH,Calendar.FEBRUARY);
                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                    if (balance.getEndDate().before(new GregorianCalendar(2023, Calendar.FEBRUARY, 28).getTime())){
                        balance.setIsValid(false);
                    }
                    balances.add(balance);
                    break;
                case GIFT:
                    if (balance.getEndDate().before(today)){
                        balance.setIsValid(false);
                    }
                    balances.add(balance);
                    break;
                default:
                    balances.add(balance);
                    break;
            }
        }
        person.setAccount(balances);
        personRepository.save(person);
        return person;
    }

    public String distributeDeposits(Distribution distributionData) {
        Company company = companyRepository.findById(distributionData.getCompany_id()).get();
        Person person = personRepository.findById(distributionData.getUser_id()).get();
        if (company.getBalance().getAmount() < distributionData.getBalance().getAmount()){
            return "company is broke";
        } else if (company.getBalance().getType() != distributionData.getBalance().getType()){
            return "company doesnt support this type of deposit";
        }
        Balance balance = distributionData.getBalance();
        balance.setStartDate(today);
        modifyEndDate(balance);
        person.getAccount().add(balance);
        personRepository.save(person);
        return "distribution ok";
    }

    public void prePersist(List<Person> people){
        for(Person user : people){
            for(Balance balance: user.getAccount()){
                modifyEndDate(balance);
            }
        }
    }

    private void modifyEndDate (Balance balance){
        LocalDateTime localDateTimePlusOneYear = today.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusYears(1);
        if (balance.getType().equals(WalletTypeEnum.FOOD)){
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR,localDateTimePlusOneYear.getYear());
            calendar.set(Calendar.MONTH,Calendar.FEBRUARY);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            balance.setEndDate(calendar.getTime());
        } else if (balance.getType().equals(WalletTypeEnum.GIFT)){
            balance.setEndDate(Date.from(localDateTimePlusOneYear.atZone(ZoneId.systemDefault()).toInstant()));
        }
        balance.setIsValid(true);
    }

}
