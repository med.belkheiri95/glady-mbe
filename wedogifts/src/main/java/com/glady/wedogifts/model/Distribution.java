package com.glady.wedogifts.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter@Setter@AllArgsConstructor@NoArgsConstructor
public class Distribution  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_balance_id", referencedColumnName = "id")
    private Balance balance;
    private Long user_id;
    private Long company_id;

}
