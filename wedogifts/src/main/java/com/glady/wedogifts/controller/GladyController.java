package com.glady.wedogifts.controller;

import com.glady.wedogifts.model.*;
import com.glady.wedogifts.repository.CompanyRepository;
import com.glady.wedogifts.repository.PersonRepository;
import com.glady.wedogifts.service.GladyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@RestController
public class GladyController {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    GladyService gladyService;

    @GetMapping("/getUsers")
    public List<Person> getUsers(){
        return personRepository.findAll();
    }

    @GetMapping("/calculateUserBalance/{id}")
    public Person calculateUserBalance(@PathVariable(value="id")  String id){
        return gladyService.calculateUserBalance(id);
    }

    @PostMapping("/saveUser")
    public ResponseEntity<String> saveUser(@RequestBody List<Person> userData) {
        gladyService.prePersist(userData);
        personRepository.saveAll(userData);
        return ResponseEntity.ok("Data saved");
    }

    @PostMapping("/saveCompanies")
    public ResponseEntity<String> saveCompanies(@RequestBody List<Company> companyData) {
        companyRepository.saveAll(companyData);
        return ResponseEntity.ok("Data saved");
    }

    @PostMapping("/distributeVoucher")
    public ResponseEntity<String> saveDistribution(@RequestBody Distribution distributionData) {
        return ResponseEntity.ok(gladyService.distributeDeposits(distributionData));
    }
}
